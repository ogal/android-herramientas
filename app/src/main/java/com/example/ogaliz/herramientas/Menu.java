package com.example.ogaliz.herramientas;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class Menu extends Fragment {

    //identificamos los id de los botones de menú
    private final int[] BOTONESMENU = {R.id.linterna, R.id.musica, R.id.nivel};


    public Menu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mimenu = inflater.inflate(R.layout.fragment_menu, container, false);

        ImageButton botonMenu;

        for (int i =0; i< BOTONESMENU.length; i++){

            botonMenu = (ImageButton) mimenu.findViewById(BOTONESMENU[i]);

            final int queBoton = i;

            //Ponemos los botones a la escucha utilizando la interfaz OnClickListener
            botonMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Tenemos en que actividad nos encontramos al pulsar el Boton
                    Activity estaActividad = getActivity();

                    ((ComunicaMenu)estaActividad).menu(queBoton);
                }
            });
        }


        return mimenu;


    }

}